﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour {
    
    public void NextLevelButton(string levelName)
    {
        SceneManager.LoadScene(levelName); //load scene based on name
    }
 
}

