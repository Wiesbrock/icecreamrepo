﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerInterActions : MonoBehaviour
{
    public Sprite[] coneSprites;
    public Sprite[] flavorSprites;
    public Sprite[] toppingSprites;

    public ConeOrder orderPrefab1;
    public ConeOrder orderPrefab2;
    public ConeOrder orderPrefab3;
    private void Start()
    {

        createOrder();
    }

    //generate order
    public void createOrder()
    {
        coneSprites = Resources.LoadAll<Sprite>("cones");
        flavorSprites = Resources.LoadAll<Sprite>("flavors");
        toppingSprites = Resources.LoadAll<Sprite>("toppings");
        Debug.Log(coneSprites.Length);
        //directly drop sprites on to canvas based on random nums?
        //randomize num scoops first to select correct prefab
        int numScoops = randomNumber(2)+1;
        //instantiate appropriate gameobject based on number of scoops

        GameObject newCone = new GameObject();
        //random cone type
        int coneType = randomNumber(0);
        newCone.AddComponent<SpriteRenderer>();
        newCone.GetComponent<SpriteRenderer>().sprite = coneSprites[coneType];
        newCone.transform.SetPositionAndRotation(new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
        newCone.GetComponent<SpriteRenderer>().sortingOrder = 3;
        //order in layer 3
        //generate random flavor
        int flavor = randomNumber(1);
        GameObject newFlavor = new GameObject();
        newFlavor.AddComponent<SpriteRenderer>();
        newFlavor.GetComponent<SpriteRenderer>().sprite = flavorSprites[flavor];
        newFlavor.transform.SetPositionAndRotation(new Vector3(0, (float)0.4, 0), new Quaternion(0, 0, 0, 0));
        newFlavor.GetComponent<SpriteRenderer>().sortingOrder = 2;
        //order in layer 2
        //position y = .4
        //toppings
        int toppings = randomNumber(3);
        GameObject newTopping = new GameObject();
        newTopping.AddComponent<SpriteRenderer>();
        newTopping.GetComponent<SpriteRenderer>().sprite = toppingSprites[toppings];
        newTopping.transform.SetPositionAndRotation(new Vector3(0, (float)0.7, 0), new Quaternion(0, 0, 0, 0));
        newTopping.GetComponent<SpriteRenderer>().sortingOrder = 3;
        //order in layer 3
        //position = y=.7
        //scale x and y = .5

    }
    
    
    //generate and return a random number for randomizing orders. type parameter represents portion of order, 0 = cone type, 1 = flavor, 
    //2 = numScoops, 3 = topping type
   int randomNumber(int type)
    {
        int randNumGenerated=-1;
        switch (type)
        {
            //cone type
            case 0:
                randNumGenerated = Random.Range(0, coneSprites.Length);
                return randNumGenerated;
            //flavor
            case 1:
                randNumGenerated = Random.Range(0, flavorSprites.Length);
                return randNumGenerated;
            //numScoops
            case 2:
                randNumGenerated = Random.Range(0, 3);
                return randNumGenerated;
            //topping type
            case 3:
                randNumGenerated = Random.Range(0, toppingSprites.Length);
                return randNumGenerated;

        }
        return randNumGenerated;

    }


}
