﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coneConfirm : MonoBehaviour
{
	public coneSelect cone;
	SpriteRenderer coneColor;
	public bool coneSelected;

	void Start()
	{
		coneColor = GetComponent<SpriteRenderer>();
		coneSelected = false;
	}

	void OnMouseEnter()
	{
		if(cone.conePicked == true)
		{
			Debug.Log("Cone Picked");
			//change alpha & color
			coneColor.color = new Color(1f, 1f, 1f, 1f);
			//set bool to true
			coneSelected = true;
		} else
		{
			coneSelected = false;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Scoop" && coneSelected == true)
		{
			coneColor.color = new Color(1f, 1f, 1f, .25f);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag == "Scoop" && coneSelected == true)
		{
			coneColor.color = new Color(1f, 1f, 1f, 1f);
		}
	}
}
