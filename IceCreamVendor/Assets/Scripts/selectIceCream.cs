﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selectIceCream : MonoBehaviour
{
	SpriteRenderer selected;
	SpriteRenderer scooped;

	public Sprite newScoop;

	itemSelect bucketBool;

	public bool canScoop;

	void Start()
    {
		selected = GetComponent<SpriteRenderer>();
		selected.enabled = false;
		canScoop = false;

		scooped = GameObject.Find("scooped").GetComponent<SpriteRenderer>();
		scooped.enabled = false;

		bucketBool = GameObject.Find("scoopInWater").GetComponent<itemSelect>();
    }

	void OnMouseOver()
	{
		if(bucketBool.itemReturned == false)
		{
			selected.enabled = true;
			canScoop = true;
		}
	}

	void OnMouseExit()
	{
		selected.enabled = false;
		canScoop = false;
	}

	void OnMouseDown()
	{
		if (canScoop == true)
		{
			scooped.enabled = true;
			scooped.sprite = newScoop;
		}
	}

	void Update()
	{
		if(bucketBool.itemReturned == true)
		{
			scooped.enabled = false;
		}
	}
}
