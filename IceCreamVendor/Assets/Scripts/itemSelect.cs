﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemSelect : MonoBehaviour
{
	public Sprite newSprite;
	public Sprite oldSprite;
	SpriteRenderer water;

	public bool itemReturned;

    void Start()
    {
		water = GetComponent<SpriteRenderer>();
		itemReturned = true;
	}

	void OnMouseDown()
	{
		if (gameObject.tag == "Scoop" && itemReturned == true)
		{
			//switch water sprite & set bool to false
			water.sprite = newSprite;
			itemReturned = false;
		}
		else if (gameObject.tag == "Scoop" && itemReturned == false)
		{
			water.sprite = oldSprite;
			itemReturned = true;
		}
	}
}
