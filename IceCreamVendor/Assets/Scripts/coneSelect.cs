﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coneSelect : MonoBehaviour
{
	private Vector3 mousePosition;
	public float moveSpeed = 0.1f;
	public SpriteRenderer coneDrag;
	public coneConfirm locked;

	public bool conePicked;

	public SpriteRenderer silhouette;

	void Start()
	{
		gameObject.SetActive(true);
		conePicked = false;
	}

	void OnMouseDown()
	{
		conePicked = true;
	}

	void Update()
	{     
        if (conePicked == true)
		{
			coneDrag.enabled = true;
			silhouette.enabled = true;

		}
		else
		{
			coneDrag.enabled = false;
			silhouette.enabled = false;
		}

		if (locked.coneSelected == true)
		{
			gameObject.SetActive(false);
		}
		
	}
}


