﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemDrag : MonoBehaviour
{
	private Vector3 mousePosition;
	public float moveSpeed = 0.1f;
	itemSelect selectBool;
	SpriteRenderer scooper;

	void Start()
	{
		selectBool = GameObject.Find("scoopInWater").GetComponent<itemSelect>();
		scooper = GameObject.Find("iceCreamScoop").GetComponent<SpriteRenderer>();
    }

    void Update()
    {
		if (selectBool.itemReturned == false)
		{
			//turn on the scooper sprite and its collider if the scooper is not in the bucket
			scooper.enabled = true;
		}
		else
		{
			//turn off the scooper sprite and its collider if the scooper is in the bucket
			scooper.enabled = false;
		}

		//this is where the scoop will attach to the pointer
		if (Input.GetMouseButton(0))
		{
			mousePosition = Input.mousePosition;
			mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
			transform.position = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
		}
	}
}
